### Full Stack Developer (Django, React, TypeScript, MongoDB, Node.js)

**Job Overview:**

We are looking for an experienced **Full Stack Developer** to join our dynamic team. You will be responsible for designing, developing, and maintaining a web application using modern technologies such as **Django, React, TypeScript, MongoDB, and Node.js**. You will collaborate with designers, backend and frontend developers, and other stakeholders to ensure the product is scalable, reliable, and easy to use.

**Key Responsibilities:**

- **Develop and maintain web applications** using Django (backend), React (frontend), TypeScript, MongoDB, and Node.js.
- **Translate Figma designs into functional frontends**, ensuring smooth user interfaces and seamless functionality.
- Implement **RESTful APIs** and ensure effective integration between the backend (Django, MongoDB) and frontend (React).
- Write clean, maintainable, and scalable code with a focus on high performance and security.
- Implement **unit testing** (both frontend and backend), including **frontend testing** using frameworks like Jest, React Testing Library, etc.
- Set up and maintain **CI/CD pipelines** using GitHub and other tools to automate tests, builds, and deployments.
- Collaborate with designers, product managers, and other developers to create a seamless development process.
- Manage issues and tasks using platforms like **GitHub Projects** or other issue management tools.
- Participate in code reviews, pair programming, and agile methodologies within a **team-oriented environment**.

**Requirements:**

- Proven experience as a Full Stack Developer, with expertise in **Django** (Python) and **React**.
- Proficiency in **TypeScript**, **JavaScript**, and **Node.js**.
- Experience with **MongoDB** and database management.
- Strong experience with **Figma** for design collaboration and UI implementation.
- Solid understanding of **version control** (Git) and using **GitHub** for team collaboration.
- Experience with **unit testing** frameworks like **Jest**, **React Testing Library**, **PyTest**, etc.
- Ability to set up and maintain **CI/CD pipelines** (GitHub Actions or similar tools).
- Strong problem-solving skills, attention to detail, and a focus on delivering high-quality, well-tested code.
- Excellent communication and collaboration skills with a demonstrated ability to work in a **team environment**.
- Experience with **Agile methodologies** is a plus.

**Nice to Have:**

- Experience with **Docker** and containerization.
- Familiarity with cloud platforms like **AWS** or **Google Cloud**.
- Knowledge of **WebSockets** or real-time web applications.
- Experience with **GraphQL**.

**Location:** Remote / In-office  
**Type:** Full-time / Contract  
**Salary:** Competitive, based on experience

**How to Apply:**

If you're passionate about building robust web applications and want to work in a collaborative environment, we'd love to hear from you! Please send your resume, portfolio, and any relevant links (GitHub, LinkedIn) to [email@example.com].