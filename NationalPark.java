import java.util.Scanner;

public class NationalPark{
	public static void main(String[] args){
		Scanner kb = new Scanner(System.in);
		Cat[] litter = new Cat[4];
		
		for(int i = 0; i < litter.length; i++){
			litter[i] = new Cat();
			
			System.out.println("Is the cat sleeping (true/false)");
			litter[i].isSleeping = Boolean.parseBoolean(kb.nextLine());
			
			System.out.println("What breed is the cat?");
			litter[i].breed = kb.nextLine();
			
			System.out.println("What is the cat's name?");
			litter[i].name = kb.nextLine();
		}
		
		System.out.println("isSleeping = " + litter[3].isSleeping);
		System.out.println("breed = " + litter[3].breed);
		System.out.println("name = " + litter[3].name);
		
		litter[0].meow();
		litter[0].purr();
	}
}