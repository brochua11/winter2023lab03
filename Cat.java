public class Cat{
	public boolean isSleeping;
	public String breed;
	public String name;
	
	public void purr(){
		System.out.println(this.name + " - rrrrrr");
	}
	
	public void meow(){
		if(!isSleeping)
			System.out.println(this.name + " - MEOW!!");
	}
	
}